from sklearn import datasets
import pandas as pd

cal = datasets.california_housing
cal = cal.fetch_california_housing()
X = cal.data
y = cal.target
desc = cal.DESCR
feat = cal.feature_names

df = pd.DataFrame(X, columns=feat)
df.assign(target = y)

df.to_csv('california_housing.csv')