# -*- coding: utf-8 -*-
"""
Created on Fri Sep 29 11:19:55 2017

@author: jfranse
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

#function for doing PCA/t-SNE

#functions for the different plots
def histograms_and_cov():
    """
    cov(r,y), cov(r,yhat) as annotations
    Histogram of r; fit gaussian; show mu, sigma
    Histogram of normalized(r)^2; show chi2 with 1 dof; (if X~N(0,1), X^2~chi^2(k=1))
    """
    pass

def r_of_y_and_yhat():
    pass

def oneD_plots():
    pass


#main 
def analyse_residuals(df, ycol=None, yhatcol=None, rcol=None):
    """pass it a dataframe, and tell it which column names are the truth and the predictions"""
    #check the dataframe and col names
    #need at least 2 out of 3 cols that exist in pd
    if sum([n in df for n in [ycol, yhatcol, rcol]]) >= 2:
        pass
    else:
        print("error: cannot find the given column names")
        return 0
    #get the number of variables
    #calculate residuals
    #call PCA/t-SNE things if dimensionality higher than 2
    #call all the other plotting procedures
    pass

if __name__ == '__main__':
    pass